<?php


use App\Models\Item;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

/**
 * Class ItemTableSeeder
 */
class ItemTableSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        DB::table('items')->truncate();
        $this->createNodes(0, 1, 10);
    }

    /**
     * @param $parent_id
     * @param $level
     * @param null $amount
     */
    private function createNodes($parent_id, $level, $amount = null)
    {
        if($level > 5) return;

        $i = 0;

        if(! $amount) {
            $amount = rand(1, 5);
        }

        while($i++ < $amount) {

            $item = Item::create([
                'parent_id' => $parent_id,
                'name'      => 'Item ' . Str::quickRandom(10),
                'order'     => $i
            ]);

            if(rand(0, 3) > 0) {
                $this->createNodes($item->id, ++$level);
            }
        }
    }
}