function addToken(obj, token){
    return $.extend({}, obj, {'_token': token});
}

function findInTree(arr, item_id) {

    for(i in arr) {
        if(parseInt(arr[i].id) == parseInt(item_id)) {
            return arr[i];
        }

        if(arr[i].items.length > 0) {
            it = findInTree(arr[i].items, item_id);
            if(typeof it !== "undefined") {
                return it;
            }
        }
    }
}

function collapseTree(arr) {

    for(i in arr) {
        arr[i].show_items = false;
        if(arr[i].items.length > 0) {
            collapseTree(arr[i].items);
        }
    }
}

function expandTree(arr) {

    for(i in arr) {
        arr[i].show_items = true;
        if(arr[i].items.length > 0) {
            expandTree(arr[i].items);
        }
    }
}

function removeFromTree(arr, item_id) {

    for(var i in arr) {
        var item = jQuery.extend({}, arr[i]);
        if(arr[i].id == item_id) {
            arr.splice(i, 1);
            return;
        }

        if(arr[i].items.length > 0) {
            removeFromTree(arr[i].items, item_id);
        }
    }
}

var myApp = angular.module('myApp', ['ui.sortable', 'toastr'])
    .constant("myConfig", {
        "CSRF_TOKEN": csrf_token
    })
    .controller('TreeController', function ($scope, $http, toastr, $q, myConfig) {

        $scope.items = Laravel.items;

        $scope.sortableOptions = {

            connectWith: "ul.drop-container",

            handle: '.handle',

            placeholder: 'placeholder-blocked',

            update: function (e, ui) {
                item = ui.item.sortable.model;
                if(typeof item === "undefined") {
                    ui.item.sortable.cancel();
                }
            },
            stop: function (e, ui) {

                if(typeof ui.item.sortable.droptarget !== "undefined") {
                    item = ui.item.sortable.model;

                    if(typeof item !== "undefined") {
                        parent = ui.item.sortable.droptarget.parents('li').first();
                        parent_id = (typeof parent.attr('id') !== "undefined") ? parent.attr('id').replace('item-', '') : 0;

                        if(parent_id > 0) {
                            parent_item = findInTree($scope.items, parent_id);
                        }

                        if(typeof parent_item !== "undefined") {
                            parent_item.show_items = true;
                        }

                        items = $('ul#list-' + parent_id + ' > li');
                        order = (items.index($('li#item-' + item.id)) + 1);

                        item.order = order;
                        item.parent_id = parent_id;
                        $scope.saveItem(item).then(
                            function (greeting) {
                                toastr.success("Element: " + item.name, "Zapisano zmiany");
                            },
                            function (reason) {
                                toastr.error("Błąd podczas zapisu");
                            }
                        );
                    }
                }
            }
        };

        $scope.addItem = function(parent_id) {
            item = {
                id:         0,
                parent_id:  0,
                name:       '',
                items:      [],
                is_edited:  true
            };

            if(typeof parent_id === "undefined") {
                $scope.items.unshift(item);
            } else {
                parent_item = findInTree($scope.items, parent_id);
                if(typeof parent_item !== "undefined") {
                    item.parent_id = parent_id;
                    parent_item.items.unshift(item);
                    parent_item.show_items = true;
                } else {
                    toastr.warning("Nie znaleziono elementu nadrzędnego");
                }
            }
        };

        $scope.saveItem = function(item) {
            var deferred = $q.defer();
            $http.post('/items' + ((item.id) ? '/' + item.id : ''), addToken(item, myConfig.CSRF_TOKEN))
                .success(function(saved_item) {
                    if(item.id == 0) {
                        item.id = saved_item.id;
                    }
                    item.is_edited = false;
                    deferred.resolve('saved');
                })
                .error(function() {
                    deferred.reject('error');
                });
            return deferred.promise;
        };

        $scope.saveName = function(item) {

            var previous_name = item.name;
            item.name = item.tmp_name;

            $scope.saveItem(item).then(
                function(greeting) {
                    toastr.success("Element: " + item.name, "Zmieniono nazwę elementu");
                },
                function(reason) {
                    toastr.error("Błąd podczas zapisu");
                    item.name = previous_name;
                }
            );
        };

        $scope.restoreName = function(item) {
            item.is_edited = false;
        };

        $scope.editItem = function(item) {
            item.tmp_name = item.name;
            item.is_edited = true;
        };

        $scope.destroyItem = function(item) {
            var deferred = $q.defer();
            $http.delete('/items/' + item.id, {params: addToken({}, myConfig.CSRF_TOKEN)})
                .success(function () {
                    deferred.resolve('deleted');
                    removeFromTree($scope.items, item.id);
                })
                .error(function () {
                    deferred.reject('error');
                });
            return deferred.promise;
        };

        $scope.removeItem = function(item) {
            $scope.destroyItem(item).then(
                function(greeting) {
                    toastr.success("Element: " + item.name, "Usunięto element");
                },
                function(reason) {
                    toastr.error("Błąd podczas usuwania");
                }
            );
        };

        $scope.collapseAll = function() {
            collapseTree($scope.items);
        };

        $scope.expandAll = function() {
            expandTree($scope.items);
        };

        $scope.toggleItems = function(item) {
            item.show_items = ((typeof item.show_items === "undefined" || ! item.show_items)) ? true : false;
        };
    });