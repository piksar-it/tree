<?php namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Routing\Controller as BaseController;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as JS;
use App\Models\Item;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

/**
 * Class TreeController
 * @package App\Http\Controllers
 */
class TreeController extends BaseController
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $items = Item::where('parent_id', '=', 0)
                     ->with(['items' => function($item) {
                         $item->orderBy('order');
                     }])
                     ->orderBy('order')
                     ->get();

        JS::put([
            'items' => $items
        ]);

        return view('tree.index');
    }

    /**
     * @return string
     */
    public function store()
    {
        try {
            $item = new Item;
            $input = Input::all();

            $parent_id = array_get($input, 'parent_id', null);
            if ($parent_id !== 0 && ! Item::find($parent_id)) {
                throw new ModelNotFoundException('Parent item not found');
            }

            DB::transaction(function() use ($item, $parent_id, $input)
            {
                $item->order     = array_get($input, 'order', 1);
                $item->name      = array_get($input, 'name', '');
                $item->parent_id = $parent_id;

                Item::where('parent_id', '=', $item->parent_id)
                    ->where('order', '>=', $item->order)
                    ->increment('order', 1);

                $item->save();
            });

            return $item->toJson();

        } catch (\Exception $e) {
            abort(404);
        }
    }


    /**
     * @param $id
     * @return string
     */
    public function update($id)
    {
        try {
            if (! $item = Item::find($id)) {
                throw new ModelNotFoundException('Item not found');
            }
            $input = Input::all();

            $parent_id = array_get($input, 'parent_id', null);
            if ($parent_id !== 0 && ! Item::find($parent_id)) {
                throw new ModelNotFoundException('Parent item not found');
            }

            DB::transaction(function() use ($item, $parent_id, $input)
            {

                Item::where('parent_id', '=', $item->parent_id)
                    ->where('order', '>', $item->order)
                    ->decrement('order', 1);

                $item->order     = array_get($input, 'order', 1);
                $item->name      = array_get($input, 'name', null);
                $item->parent_id = $parent_id;

                Item::where('parent_id', '=', $item->parent_id)
                    ->where('order', '>=', $item->order)
                    ->increment('order', 1);

                $item->save();
            });

            return $item->toJson();

        } catch (\Exception $e) {
            abort(404);
        }
    }

    /**
     * @param $parent_id
     */
    private function destroyWithSubitems($parent_id)
    {
        $item = Item::find($parent_id);

        foreach($item->items()->get() as $i) {
            $this->destroyWithSubitems($i->id);
            $i->delete();
        }
        $item->delete();
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        try {
            if (!$item = Item::find($id)) {
                throw new ModelNotFoundException('Item not found');
            }
            $this->destroyWithSubitems($item->id);

        } catch (\Exception $e) {
            abort(404);
        }
    }
}
