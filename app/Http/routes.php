<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app->get('/', [
    'as' => 'tree.index',
    'uses' => 'App\Http\Controllers\TreeController@index'
]);

$app->post('items/{id}', [
    'as' => 'tree.item.update',
    'uses' => 'App\Http\Controllers\TreeController@update'
]);

$app->post('items', [
    'as' => 'tree.item.store',
    'uses' => 'App\Http\Controllers\TreeController@store'
]);

$app->delete('items/{id}', [
    'as' => 'tree.item.store',
    'uses' => 'App\Http\Controllers\TreeController@destroy'
]);
