<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Item
 * @package App\Models
 */
class Item extends Model {

    /**
     * @var string
     */
    protected $table = 'items';

    /**
     * @var array
     */
    protected $fillable = ['parent_id', 'name', 'order'];

    /**
     * @return mixed
     */
    public function items()
    {
        return $this->hasMany('App\Models\Item', 'parent_id')->with(['items' => function($item) {
            $item->orderBy('order');
        }]);
    }
}