<li>
    {{ $item->name }}
    @if(count($item->children))
        <ul>
        @foreach($item->children as $item)
            @include('tree.partials.item', ['item' => $item])
        @endforeach
        </ul>
    @endif
</li>