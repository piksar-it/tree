@extends('layouts.default')

@section('content')
    <div ng-app="myApp" ng-controller="TreeController" id="tree">
        <script type="text/ng-template" id="itemTree">
            <div class="input-group">
                <span class="input-group-btn" ng-if="item.items.length">
                    <button ng-click="toggleItems(item)" type="button" class="btn btn-default">
                        <span ng-show="item.show_items" class="caret"></span>
                        <span ng-hide="item.show_items" class="caret gly-rotate-270"></span>
                    </button>
                </span>
                <span class="input-group-addon btn-default handle" id="basic-addon2"><span class="glyphicon glyphicon-move"></span></span>

                <input ng-show="item.is_edited" type="text" class="form-control" ng-model="item.tmp_name">
                <div ng-hide="item.is_edited" class="form-control">
                    <small class="name">(@{{ item.id }}) @{{ item.name }}</small>
                </div>
                <div class="input-group-btn">
                    <button type="button" ng-click="saveName(item)" ng-show="item.is_edited" class="btn btn-default"><span class="glyphicon glyphicon-ok text-success"></span></button>
                    <button ng-if="item.id" type="button" ng-click="restoreName(item)" ng-show="item.is_edited" class="btn btn-default"><span class="glyphicon glyphicon-remove text-danger"></span></button>
                    <button ng-if="item.id" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                    <ul ng-if="item.id" class="dropdown-menu dropdown-menu-right" role="menu">
                        <li><a href="#" ng-click="addItem(item.id)" class="add-tmp-subcategory"><span class="glyphicon glyphicon-plus"></span> Dodaj podkategorię</a></li>
                        <li><a href="#" ng-click="editItem(item)"><span class="glyphicon glyphicon-edit"></span> Zmień nazwę</a></li>
                        <li><a href="#" ng-click="removeItem(item)"><span class="glyphicon glyphicon-trash"></span> Usuń</a></li>
                    </ul>
                </div>
            </div>
            <ul ui-sortable="sortableOptions"
                class="drop-container"
                ng-model="item.items"
                id="list-@{{ item.id }}">
                <li ng-if="$parent.item.show_items"
                    ng-include="'itemTree'"
                    ng-repeat="item in item.items"
                    id="item-@{{ item.id }}">
                </li>
            </ul>
        </script>

        <div id="tree">
            <div class="row">
                <div class="col-sm-12">
                    <div class="btn-group pull-right">
                        <button ng-click="addItem()" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus"></span> Dodaj</button>
                        <button ng-click="expandAll()" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-resize-full"></span></button>
                        <button ng-click="collapseAll()" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-resize-small"></span></button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <ul ui-sortable="sortableOptions"
                        class="drop-container"
                        ng-model="items"
                        id="list-0">
                        <li ng-repeat="item in items"
                            ng-include="'itemTree'"
                            id="item-@{{ item.id }}">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('headerCss')
    <link href="/css/cover.css" rel="stylesheet">
@endsection

@section('footerScripts')
    <script src="/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="/js/sortable.min.js"></script>
    <script src="/js/tree.js"></script>
@endsection