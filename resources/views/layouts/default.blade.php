<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Tree management</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/angular-toastr.min.css" rel="stylesheet">

    @yield('headerCss')
</head>

<body>

<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">
                <div class="inner">
                    <h3 class="masthead-brand">Tree management</h3>
                    <nav>
                        <ul class="nav masthead-nav">
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="inner cover">

                @yield('content')

            </div>

            <div class="mastfoot">
                <div class="inner">
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</div>

@include('layouts.partials.js')
<script>
    var csrf_token = "{{ csrf_token() }}";
</script>
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/angular.min.js"></script>
<script src="/js/angular-toastr.tpls.min.js"></script>
@yield('footerScripts')
</body>
</html>
