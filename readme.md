## Zarządzanie strukturą drzewiastą

Projekt przedstawia strukturę drzewiastą z możliwością dodawania, edycji, usuwania oraz sortowania poszczególnych gałęzi.

Aplikacja wykonana została w technologiach PHP5 (Lumen Framework), Mysql, AngularJS

### Wymagania

PHP 5.4 lub wyższe,

Composer,

MySql,

Przeglądarka internetowa obsługująca JavaScript (nie testowane w starych wersjach przeglądarek).


### Instalacja

####1. Pobranie aplikacji z repozytorium:
    $ git clone git@bitbucket.org:piotrsarna/tree.git

####2. Konfihuracja VirtualHosts:
    Katalog domowy public

####3. Aktualizacja zależności:
    $ composer update

####4. Stworzenie pliku konfiguracyjnego (skopiować z głównego katalogu aplikacji **.env.example** z nową nazwą **.env**)
    $ copy .env.example .env

####5. Utworzenie bazy danych MySql:
    Po utworzeniu bazy należy dane do logowania wprowadzić w pliku .env

    DB_HOST=localhost
    DB_DATABASE=homestead
    DB_USERNAME=homestead
    DB_PASSWORD=secret

####5. Utworzenie struktury bazy danych wraz z przykladowymi danymi:

    $ php artisan migrate
    $ php artisan db:seed

#### DEMO

[tree.capriolo.pl](http://tree.capriolo.pl)

#### Licencja

Open-source software license [MIT license](http://opensource.org/licenses/MIT)